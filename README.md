Тестовое задание

- Создать репозиторий на GitLab
- Создать index.html h1 с текстом "Hello Admin"
- Написать Dockerfile, использовать за основу nginx:alpine
- Nginx не должен в заголовке передавать свою версию
- Написать .dockerignore - подумать над содержимым
- Написать readme.md - как собирать и запускать
- Написать pipeline сборки докер образа для загрузки его в hub.docker.com
- Написать docker-compose.yml для запуска вашего сайта:
    образ должен скачиваться с hub.docker.com
    контейнер должен автоматически перезапускаться
    настроить ротацию логов, 3 файла по 1 мб в формате json



Test job

Hello Admin html

Для сборки проекта

docker build . -t server_name

Для запуска проекта с помощью docker:

docker run -it --rm -p 8080:80 server_name

Для запуска с помощью docker-compose: 

docker-compose up -d

В браузере перейти по адресу

http://localhost:8080
